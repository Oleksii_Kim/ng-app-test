import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { CurrencyExchangerComponent } from './currency-exchanger/currency-exchanger.component';
import { CurrencyInputSelectComponent } from './currency-exchanger/currency-input-select/currency-input-select.component';
import { HttpClientModule} from "@angular/common/http";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CurrencyExchangerComponent,
    CurrencyInputSelectComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

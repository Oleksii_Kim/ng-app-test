import {Component} from '@angular/core';
import {ExchangeService} from "../core/exchange/exchange.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  readonly exchangeRates$ = this.exchangeService.getExchangeRatesByCurrencies(this.exchangeService.supportedCurrencies);

  constructor(private readonly exchangeService: ExchangeService) {
  }

}

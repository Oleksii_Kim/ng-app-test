import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-currency-input-select',
  templateUrl: './currency-input-select.component.html',
  styleUrls: ['./currency-input-select.component.css']
})
export class CurrencyInputSelectComponent {
  @Input() currencies: string[] | null = null;
  @Input() value: { amount: number, currency: string } | null = null;
  @Output() currencyAmountChange = new EventEmitter<{ amount: number, currency: string }>();

  onChangeAmount(event: Event) {
    if (this.value === null) {
      return
    }
    const amount = +(event.target as HTMLInputElement).value
    this.currencyAmountChange.emit({...this.value, amount})
  }

  onChangeCurrency(event: Event) {
    if (this.value === null) {
      return
    }
    const currency = (event.target as HTMLInputElement).value
    this.currencyAmountChange.emit({...this.value, currency})
  }
}

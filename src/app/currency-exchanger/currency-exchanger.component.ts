import {Component} from '@angular/core';
import {forkJoin, map, take} from "rxjs";
import {ExchangeService} from "../core/exchange/exchange.service";

@Component({
  selector: 'app-currency-exchanger',
  templateUrl: './currency-exchanger.component.html',
  styleUrls: ['./currency-exchanger.component.css']
})
export class CurrencyExchangerComponent {
  readonly currenciesToConvert = [...this.exchangeService.supportedCurrencies, this.exchangeService.baseCurrency]

  readonly rates$ = this.exchangeService.getExchangeRatesByCurrencies(this.currenciesToConvert)

  valueFirst = {amount: 0, currency: 'USD'}
  valueSecond = {amount: 0, currency: 'UAH'}

  constructor(private readonly exchangeService: ExchangeService) {
  }

  currencyAmountChangeFirst(event: { amount: number, currency: string }) {
    this.valueFirst = event
    this.getRate(event.currency, this.valueSecond.currency).pipe(take(1)).subscribe((rate) => {
      if (rate === null) {
        return
      }

      this.valueSecond = {...this.valueSecond, amount: event.amount * rate}
    })
  }

  currencyAmountChangeSecond(event: { amount: number, currency: string }) {
    this.valueSecond = event
    this.getRate(event.currency, this.valueFirst.currency).pipe(take(1)).subscribe((rate) => {
      if (rate === null) {
        return
      }

      this.valueFirst = {...this.valueFirst, amount: event.amount * rate}
    })
  }

  private getRate(currencyFrom: string, currencyTo: string) {
    return forkJoin([this.getRateByCurrency(currencyFrom), this.getRateByCurrency(currencyTo)]).pipe(map(([rateFrom, rateTo]) => {
      if (rateFrom !== null && rateTo !== null) {
        return rateFrom / rateTo;
      }

      return null
    }))
  };

  private getRateByCurrency(currency: string) {
    return this.rates$.pipe(map(rates => {
      const rate = rates.find(item => item.currency === currency)
      if (!rate) {
        return null
      }

      return rate.rate
    }))
  };
}

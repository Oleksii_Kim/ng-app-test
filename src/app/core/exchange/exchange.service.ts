import {Injectable} from '@angular/core';
import {map} from "rxjs";
import {ExchangeDataService} from "./exchange-data.service";

@Injectable({
  providedIn: 'root'
})
export class ExchangeService {
  readonly supportedCurrencies = ['USD', 'EUR'];
  readonly baseCurrency = 'UAH';
  readonly baseCurrencyRate = {
    currency: this.baseCurrency,
    rate: 1,
  }

  constructor(private _dataCurrency: ExchangeDataService) {
  }

  getExchangeRatesByCurrencies(currencies: string[]) {
    return this._dataCurrency.getExchangeRates().pipe(map(exchangeRates => {
        const rates = exchangeRates.filter(item => this.supportedCurrencies.includes(item.currency))

        return currencies.includes(this.baseCurrency) ? [...rates, this.baseCurrencyRate] : rates;
      }
    ))
  }
}

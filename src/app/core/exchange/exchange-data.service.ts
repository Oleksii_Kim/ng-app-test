import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ExchangeRate} from "./exchange-rate";
import {map, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ExchangeDataService {
  private readonly url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'

  constructor(private httpClient: HttpClient) {
  }

  getExchangeRates(): Observable<ExchangeRate[]> {
    return this.httpClient.get<{
      cc: string
      exchangedate: string
      r030: number
      rate: number
      txt: string
    }[]>(this.url).pipe(map(rates => rates.map(({cc, rate}) => ({rate, currency: cc}))));
  }
}
